package com.example.fatma.demo;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class room_adapter extends RecyclerView.Adapter<room_adapter.MyViewHolder> {

    private Context context;
    private ArrayList<rooms_model> datalist;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView chat_title ,chat_count;
        public ImageView chat_favourite;



        public MyViewHolder(View view) {
            super(view);

            chat_title=(TextView) view.findViewById(R.id.chat_title);
            chat_count=(TextView) view.findViewById(R.id.chat_count);
            chat_favourite=(ImageView) view.findViewById(R.id.chat_favourite);

        }
    }


    public room_adapter(Context context, ArrayList<rooms_model> datalist) {
        this.context = context;
        this.datalist = (ArrayList<rooms_model>) datalist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_rooms, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        rooms_model data = datalist.get(position);


        Picasso.with(context).load(data.pic).into(holder.image);
        holder.chat_title.setText(data.name);

        holder.chat_count.setText(String.valueOf(data.number_users);






    }

    @Override
    public int getItemCount() {
        return datalist.size();
    }

}



