package com.example.fatma.demo;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class country_adapter extends RecyclerView.Adapter<country_adapter.MyViewHolder> {

    private Context context;
    private ArrayList<country_model> datalist;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView country_name,users_number,rooms_number;
        public ImageView image;


        public MyViewHolder(View view) {
            super(view);
            image=(ImageView)view.findViewById(R.id.country_image);
            country_name=(TextView) view.findViewById(R.id.country_name);
            users_number=(TextView) view.findViewById(R.id.users_numbers);
            rooms_number=(TextView) view.findViewById(R.id.rooms_numbers);




        }
    }


    public country_adapter(Context context, ArrayList<country_model> datalist) {
        this.context = context;
        this.datalist = (ArrayList<country_model>) datalist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.country_item, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        country_model data = datalist.get(position);
        Log.w("bataaaaaaaah",data.pic);
        holder.country_name.setText(data.country);
        holder.rooms_number.setText(String.valueOf(data.room_number));
        holder.users_number.setText(String.valueOf(data.users_number));
        Picasso.with(context).load(data.pic).into(holder.image);





    }

    @Override
    public int getItemCount() {
        return datalist.size();
    }

}



