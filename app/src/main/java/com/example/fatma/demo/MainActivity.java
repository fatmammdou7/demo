package com.example.fatma.demo;

import android.content.Intent;
import android.graphics.Movie;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<country_model> country_modelArrayList = new ArrayList<>();
    country_adapter mv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mv = new country_adapter(this,country_modelArrayList);
        RecyclerView rvy = findViewById(R.id.rcv);
        RecyclerView.LayoutManager LayoutManager = new LinearLayoutManager(getApplicationContext());
        rvy.setLayoutManager(LayoutManager);
        rvy.setItemAnimator(new DefaultItemAnimator());
        rvy.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 5));
        rvy.setAdapter(mv);
        rvy.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), rvy, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent goto_rooms=new Intent(MainActivity.this , Rooms.class);
               finish();
               startActivity(goto_rooms);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        add_country_data();

    }
    public void add_country_data(){
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference();

// Attach a listener to read the data at our posts reference
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                country_modelArrayList.clear();
                for (DataSnapshot countries:dataSnapshot.getChildren()){
                   country_model country =new country_model(countries.child("country").getValue(String.class),countries.child("pic").getValue(String.class)
                            ,countries.child("room_number").getValue(Integer.class),countries.child("users_number").getValue(Integer.class));
                    country_modelArrayList.add(country);

                }
                mv.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });
    }
}
